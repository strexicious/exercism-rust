pub fn reply(message: &str) -> &str {
    let t_message = message.trim();
    let c_uppercase =
        t_message == t_message.to_uppercase() && t_message.contains(char::is_alphabetic);

    if c_uppercase && t_message.ends_with("?") {
        "Calm down, I know what I'm doing!"
    } else if t_message.is_empty() {
        "Fine. Be that way!"
    } else if t_message.ends_with("?") {
        "Sure."
    } else if c_uppercase {
        "Whoa, chill out!"
    } else {
        "Whatever."
    }
}
