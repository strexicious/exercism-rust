pub fn build_proverb(list: Vec<&str>) -> String {
    let mut ret = String::new();
    if !list.is_empty() {
        for i in 0..list.len()-1 {
            ret += &format!("For want of a {} the {} was lost.\n", list[i], list[i+1]);
        }
        ret += &format!("And all for the want of a {}.", list[0]);
    }
    ret
}
