use std::string::String;
use std::iter::FromIterator;

pub fn reverse(input: &str) -> String {
    let rchars = input.chars().rev();
    String::from_iter(rchars)
}
