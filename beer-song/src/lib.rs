pub fn verse(n: i32) -> String {
    if n == 0 {
        String::from("No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n")
    } else if n == 1 {
        String::from("1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n")
    } else if n == 2 {
        format!("{a} bottles of beer on the wall, {a} bottles of beer.\nTake one down and pass it around, {b} bottle of beer on the wall.\n", a=n, b=n-1)
    } else {
        format!("{a} bottles of beer on the wall, {a} bottles of beer.\nTake one down and pass it around, {b} bottles of beer on the wall.\n", a=n, b=n-1)
    }
}

pub fn sing(start: i32, end: i32) -> String {
    let mut s = String::new();
    for n in (end .. start+1).rev() {
        s.extend(verse(n).chars());
        if n != end {
            s += "\n";
        }
    }

    s
}
