pub fn nth(n: u32) -> Option<u32> {
    if n == 0 {
       return None;
    }

    let mut primes: Vec<u32> = vec![2, 3, 5, 7, 11];
    let mut next_pos = 13;

    let n = n as usize;
    while n > primes.len() {
        let mut is_prime = true;
        
        for p in primes.iter().take_while(|x| x.pow(2) <= next_pos) {
            if next_pos % p == 0 {
                is_prime = false;
                break;
            }
        }

        if is_prime {
            primes.push(next_pos);
        }

        next_pos += 2;
    }

    primes.pop()
}