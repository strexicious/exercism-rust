use std::string::String;

pub fn raindrops(n: usize) -> String {
    let mut rs = String::new();

    if n % 3 == 0 {
        rs += "Pling";
    }
    if n % 5 == 0 {
        rs += "Plang";
    }
    if n % 7 == 0 {
        rs += "Plong";
    }

    if rs.is_empty() {
        n.to_string()
    } else {
        rs
    }
}
